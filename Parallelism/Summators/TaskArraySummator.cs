﻿namespace Parallelism.Summators
{
    internal class TaskArraySummator : IArraySummator
    {
        private const int TaskCount = 7;
        private readonly int _chunkSize;
        private readonly int _lastChunkExcess;
        private readonly byte[] _array;

        internal TaskArraySummator(byte[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentException("Array is undefined or empty");
            }
            _array = array;
            _chunkSize = array.Length / TaskCount;
            _lastChunkExcess = array.Length % TaskCount;
        }

        public int ArrayLength => _array.Length;

        public ulong SumArray()
        {
            ulong totalSum = 0;
            void SumSubArray(int startIndex, int finishIndex)
            {
                ulong subArraySum = 0;
                for (var i = startIndex; i <= finishIndex; i++)
                {
                    subArraySum += _array[i];
                }
                Interlocked.Add(ref totalSum, subArraySum);
            }

            var tasks = new Task[TaskCount];
            for (var i = 0; i < TaskCount; i++)
            {
                var startIndex = _chunkSize * i;
                var finishIndex = startIndex + (_chunkSize - 1);

                if (i == TaskCount - 1)
                {
                    finishIndex += _lastChunkExcess;
                }
                var task = new Task(() => SumSubArray(startIndex, finishIndex));
                task.Start();
                tasks[i] = task;
            }
            Task.WaitAll(tasks);
            return totalSum;
        }
    }
}