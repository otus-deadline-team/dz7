﻿namespace Parallelism.Summators
{
    internal class SequentialArraySummator : IArraySummator
    {
        private readonly byte[] _array;

        internal SequentialArraySummator(byte[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentException("Array is undefined or empty");
            }
            _array = array;
        }

        public int ArrayLength => _array.Length;

        public ulong SumArray()
        {
            ulong result = 0;
            for (var i = 0; i < _array.Length; i++)
            {
                result += _array[i];
            }
            return result;
        }
    }
}