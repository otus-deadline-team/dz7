﻿namespace Parallelism.Summators
{
    internal class TplArraySummator : IArraySummator
    {
        private const int ThreadCount = 7;
        private readonly byte[] _array;

        internal TplArraySummator(byte[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentException("Array is undefined or empty");
            }
            _array = array;
        }

        public int ArrayLength => _array.Length;

        public ulong SumArray()
        {
            ulong totalSum = 0;
            Parallel.ForEach(
                _array,
                new ParallelOptions { MaxDegreeOfParallelism = ThreadCount },
                () => (ulong) 0,
                (element, _, localSum) =>
                {
                    localSum += element;
                    return localSum;
                },
                localSum => Interlocked.Add(ref totalSum, localSum)
            );
            
            return totalSum;
        }
    }
}