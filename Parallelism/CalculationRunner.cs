﻿using System.Diagnostics;
using Parallelism.Summators;

namespace Parallelism
{
    internal static class CalculationRunner
    {
        internal static void Run(IArraySummator summator)
        {
            Console.WriteLine($"Summing {summator.ArrayLength:N0} elements via '{summator.GetType().Name}' - starting");

            var stopwatch = Stopwatch.StartNew();

            var result = summator.SumArray();
            
            stopwatch.Stop();

            Console.WriteLine($"Summing {summator.ArrayLength:N0} elements via '{summator.GetType().Name}' - finished (result: {result}, time elapsed: {stopwatch.ElapsedMilliseconds} ms)");
            Console.WriteLine();
        }
    }
}