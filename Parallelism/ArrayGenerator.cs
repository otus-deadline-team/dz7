﻿namespace Parallelism
{
    internal class ArrayGenerator
    {
        private readonly uint _arrayLength;

        internal ArrayGenerator(uint arrayLength)
        {
            _arrayLength = arrayLength;
        }

        internal byte[] Generate()
        {
            var array = new byte[_arrayLength];
            
            new Random().NextBytes(array);

            return array;
        }
    }
}
